# README

> Custom implementation of Unix shell in C. 

## Features

1. Input output redirection
2. Piping
3. Background processes
4. Signal handling
5. Environment variables
6. Custom commands listed below

To compile: `make`

To run: `./main`

 ### Assumptions

 - The directory from where the shell is launched will be set as the root directory for the shell.
 - It is assumed that background process logic should only work on commands executed by execvp() since other commands are custom and execute instantly.
 - The path should not contain any spaces.

## Files 

- **cd.c** 
- **childproc.c** 
- **cleanup.c** 
- **definitions.h** 
- **echo.c** 
- **envvar.c** 
- **execute.c** 
- **fgbg.c** 
- **generics.c** 
- **globals.c** 
- **headers.h** 
- **history.c** 
- **init.c** 
- **jobs.c** 
- **log.txt**
- **ls.c** 
- **main.c** 
- **makefile** 
- **pinfo.c** 
- **prompt.c** 
- **pwd.c** 
- **shellinput.c** 

