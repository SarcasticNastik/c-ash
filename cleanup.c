#include "definitions.h"
#include "globals.h"
#include "headers.h"

void cleanup() {
  printf("> Exiting . . .\n");
  char *homeDir = getHomeDir();
  free(homeDir);

  char **historyList = getHistoryList();
  free(historyList);

  processDetails *temp, *head;
  temp = head = getChildProcessList();
  while ((temp->next) != NULL) {
    processDetails *node = temp->next;
    kill(node->pid, SIGKILL);
    printf("> Killed %s with pid %d\n", node->pname, node->pid);
    head = temp;
    temp = temp->next;
    free(head);
  }
  free(head);
  printf("\n------------------------------------------------------------\n\n");
  exit(0);
}
